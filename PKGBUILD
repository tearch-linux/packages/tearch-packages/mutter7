pkgname=mutter7
pkgver=3.38.5
pkgrel=3
pkgdesc="A window manager for GNOME"
url="https://gitlab.gnome.org/GNOME/mutter"
arch=(x86_64)
license=(GPL)
depends=(dconf gobject-introspection-runtime gsettings-desktop-schemas
         libcanberra startup-notification zenity libsm gnome-desktop upower
         libxkbcommon-x11 gnome-settings-daemon libgudev libinput pipewire
         xorg-xwayland graphene)
makedepends=(gobject-introspection git egl-wayland meson xorg-server sysprof wayland-protocols)
checkdepends=(xorg-server-xvfb)
provides=(libmutter-7.so)
_commit=6fd13704c70fe92c2195bb92efd38e172082de55  # tags/3.38.5^0
source=("git+https://gitlab.gnome.org/GNOME/mutter.git#commit=$_commit"
	"0001-window-actor-Special-case-shaped-Java-windows.patch"
	"0002-Adapt-to-settings-moving-to-gsettings-desktop-schema.patch"
	"0003-Fix-glitches-in-gala.patch")
sha256sums=('SKIP' 'SKIP' 'SKIP' 'SKIP')

pkgver() {
  cd mutter
  git describe --tags | sed 's/-/+/g'
}

prepare() {
  cd mutter

  patch -Np1 -i ../0001-window-actor-Special-case-shaped-Java-windows.patch
  patch -Np1 -i ../0002-Adapt-to-settings-moving-to-gsettings-desktop-schema.patch
  patch -Np1 -i ../0003-Fix-glitches-in-gala.patch
}

build() {
  CFLAGS="${CFLAGS/-O2/-O3} -fno-semantic-interposition"
  LDFLAGS+=" -Wl,-Bsymbolic-functions"
  arch-meson mutter build \
    --libexecdir /usr/lib/mutter-7 \
    -D egl_device=true \
    -D installed_tests=false \
    -D profiler=false \
    -D wayland_eglstream=true \
    -D xwayland_initfd=disabled
  meson compile -C build
}

package() {
  DESTDIR="$pkgdir" meson install -C build
  rm -rf "${pkgdir}"/usr/{bin,share} "${pkgdir}"/usr/lib/udev/rules.d/61-mutter.rules
}
